// We setup the main Angular model that we will use for our application

var crimeMapApp = angular.module('crimeMap', ['ngRoute','ngResource', 'ngCookies', 'ngMap', 'firebase']);
// Here we configure our application module and more specifically our $routeProvider.

//Might be completely unneccesary
crimeMapApp.config(function($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
        // Allow same origin resource loads.
        'self',
        'https://maps.googleapis.com/maps/api/js/**']);
});

crimeMapApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/home', {
                styleUrls: ['style.css'],
                templateUrl: 'partials/home.html',
                controller: 'HomeCtrl'

            }).
            when('/map', {
                templateUrl: 'partials/map.html',
                controller: 'MapCtrl'
            }).

            when('/login', {
                templateUrl: 'partials/login.html',
                controller: 'LoginCtrl'
            }).

            when('/about', {
                templateUrl: 'partials/about.html'

            }).
            //not fully implemented
            when('/crimes', {
                templateUrl: 'partials/crimes.html',
                controller: 'LocationCtrl'

            }).

            otherwise({
                redirectTo: '/home'
            });


    }]);
