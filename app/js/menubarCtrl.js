//Controls menubar/header
crimeMapApp.controller('MenubarCtrl', function($scope, CrimeMap, NgMap){

    $scope.check = function() {
        return CrimeMap.checkIfLoggedIn();
    };

    $scope.loginCheck = function(){
        if (CrimeMap.checkIfLoggedIn() === true){
            $scope.login = "SIGN OUT";
            $scope.loginsv = "LOGGA UT";
        }else {
            $scope.login = "SIGN IN";
            $scope.loginsv = "LOGGA IN";
        }
    };

    CrimeMap.auth.$onAuthStateChanged(function(){
        $scope.loginCheck();
    });

});
