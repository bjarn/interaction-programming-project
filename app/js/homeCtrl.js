crimeMapApp.controller('HomeCtrl', function ($scope,CrimeMap){
    $scope.check = function() {
        return CrimeMap.checkIfLoggedIn();

    };

    $scope.loginCheck = function(){
        if (CrimeMap.checkIfLoggedIn() === true){
            $scope.id = CrimeMap.auth.$getAuth().email;

        }else {
            $scope.id = null;
        }
        return $scope.id;
    };

    CrimeMap.auth.$onAuthStateChanged(function(){
        $scope.id = $scope.loginCheck();
    });

});

