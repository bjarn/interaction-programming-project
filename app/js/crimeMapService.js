// Here we create an Angular service that we will use for our
// model. 

crimeMapApp.factory('CrimeMap',function ($resource, $cookieStore, NgMap, $firebaseAuth,$firebaseObject, $firebaseArray) {
    //For now just center the map on stockholm initially
    this.initCoordinates = [59.3374584, 18.0856606];
    //Will contain center location of mapp
    this.currentLocation =undefined;
    //Will contain tracked locations of user
    this.locations = [];
    //IDefault language is swedish since all crime-reports are in swedish
    this.language = "SE";
    
    //Top secret api-key to firebase
    var config = {
        apiKey: "AIzaSyCSpwR-xkRgG7wtue9dWjdrpul0LYMkZ10",
        authDomain: "crimap-f9019.firebaseapp.com",
        databaseURL: "https://crimap-f9019.firebaseio.com",
        storageBucket: "crimap-f9019.appspot.com",
        messagingSenderId: "99063073040"
    };
    
    //Connect to Firebase
    firebase.initializeApp(config);
    this.auth=$firebaseAuth();

    //Some CB might need this
    var self = this;

    //authentication manipulation
    this.checkIfLoggedIn = function() {
        var check = this.auth.$getAuth();
        if(check===null){
            response = false;
        }else{
            response =true;
        }
        return response;
    };
    this.signIn = function(email, password) {
        this.auth.$signInWithEmailAndPassword(email, password).catch(function(error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            if (errorCode === 'auth/wrong-password') {
                alert('Wrong password.');
            } else {
                alert(errorMessage);
            }
        });
        if(this.checkIfLoggedIn()===true){
            this.loadFromFirebase();
        }
    };
    this.signOut=function(){
        console.log(this.auth);
        this.auth.$signOut();
        this.locations = [];
    };


    //database manipulation
    this.saveToFirebase = function(){
        var uid = JSON.stringify(this.auth.$getAuth().uid);
        this.nonLocal =$firebaseObject(firebase.database().ref().child(uid));
        this.nonLocal.locationStorage=JSON.stringify(this.locations);
        this.nonLocal.$save();
    };

   this.loadFromFirebase = function(){
        var uid = JSON.stringify(this.auth.$getAuth().uid);
        this.nonLocal =$firebaseObject(firebase.database().ref().child(uid));
        this.nonLocal.$loaded().then(function(data){
            if(data!==null){
                self.locations=JSON.parse(data.locationStorage);
            }
        });
    };

    //Location manipulation
    this.getLocations=function(){
        return this.locations;
    };
    this.addLocation = function(newLocation){
        //check if it exists first
        for(var key in this.locations){
            if(this.locations[key].id == newLocation.id) {
                return;
            }
        }
        locationToAdd={
            "id":newLocation.id,
            "name":newLocation.name,
            "geometry":{
                "location":{
                    "lat":newLocation.geometry.location.lat(),
                    "lng":newLocation.geometry.location.lng()
                }
            }
        };
        this.locations.push(locationToAdd);
        if(this.checkIfLoggedIn()){
            this.saveToFirebase(this.locations);
        }
    };

    this.removeLocation = function(id){
        for(var key in this.locations){
            if(this.locations[key].id == id) {
                index = key;
                break;
            }
        }
        this.locations.splice(index,1);
        if(this.checkIfLoggedIn()){
            this.saveToFirebase(this.locations);
        }
    };

    //Cookiehandling
    this.setLocationCookie = function(){
        $cookieStore.put('currentLocation', this.currentLocation.geometry.location);
    };
    this.getLocationCookie = function(){
        return $cookieStore.get('currentLocation');
    };

    this.setLanguageCookie = function(){
        $cookieStore.put('language', this.language);
    };
    this.getLanguageCookie = function(){
        return $cookieStore.get('language');
    };

    //Languagehandling
    this.setLanguage = function(lang){
        this.language = lang;
        this.setLanguageCookie();
    };
    this.getLanguage = function(){
        var language = this.getLanguageCookie('language');
        if(language===undefined){
            return this.language;
        }else{
            return language;
        }
    };

    //Allways init on stockholm if no cookie etc
    this.getInitCoordinates = function(){
        var initCoord = this.getLocationCookie();
        if(initCoord===undefined & this.currentLocation===undefined){
            return this.initCoordinates;
        }else if(this.currentLocation!==undefined){
            return this.currentLocation.geometry.location;
        }
        else{
            return [initCoord.lat, initCoord.lng];
        }
    };

    // AJAX operations
    // Need to use a corsproxy for brottskartan API
    // The npm start script should start a local cors-proxy at port 9090
    // this.searchCloseTo = $resource('https://crossorigin.me/https://brottsplatskartan.se/api/eventsNearby',{},{
    this.searchCloseTo = $resource('http://localhost:9090/https://brottsplatskartan.se/api/eventsNearby',{},{
        get: {
        }
    });


    //Clean up tags in texts. Used for content of infowindows
    this.removeTags=function(text){
        return text.replace(/<.*>/gi,"");
    };

    //Map manipulation
    //Change center of google map
    this.setCurrentLocation = function(newLocation){
        this.currentLocation=newLocation;
        this.setLocationCookie();
        console.log(this.currentLocation.geometry.location);
        this.map.setCenter(this.currentLocation.geometry.location);
    };
    //Set the map so the dinnerservice can access it
    this.setMap = function(map){
        this.map=map;
    };
    this.getMap = function(){
        return this.map;
    };

    //Run thios callback when autstate has changed
    this.auth.$onAuthStateChanged(function(){
        if(self.checkIfLoggedIn()){
            self.loadFromFirebase();
        }
    });


    return this;
});
