crimeMapApp.controller('LocationCtrl', function ($scope, CrimeMap) {
    $scope.locations=CrimeMap.getLocations();
    $scope.locationStatus= "-";

    //Location handling
    $scope.getLocationStatus = function(){
        return $scope.locationStatus;
    };
    $scope.placeChanged = function (){
        $scope.place = this.getPlace();
        CrimeMap.setCurrentLocation($scope.place);
        $scope.getCloseByEvents($scope.place.geometry.location.lat(), $scope.place.geometry.location.lng());
    };
    $scope.placeChangedManually = function (place){
        $scope.place = place;
        CrimeMap.setCurrentLocation($scope.place);
        console.log(place);
        $scope.getCloseByEvents($scope.place.geometry.location.lat, $scope.place.geometry.location.lng);
    };

    $scope.addLocation = function(){
        console.log($scope.place);
        CrimeMap.addLocation($scope.place);
        $scope.locations=CrimeMap.getLocations();
        $scope.locationStatus= "";
    };
    $scope.getTrackedLocations = function(){
        $scope.locations = CrimeMap.getLocations();
        if($scope.locations.length>0){
            $scope.locationStatus= "";
        }else{ 
            $scope.locationStatus= "-";
        }
        return $scope.locations;
    };
    $scope.removeTrackedLocation = function(id){
        CrimeMap.removeLocation(id);
        if($scope.locations.length>0){
            $scope.locationStatus= "";
        }else{ 
            $scope.locationStatus= "-";
        }
    };
    
});
