//Global language controller, called in index.html, very hacky
crimeMapApp.controller('LanguageCtrl', function ($scope,CrimeMap){
    $scope.setLanguage = function(lang){
        CrimeMap.setLanguage(lang);
        $scope.language=lang;
    };
    $scope.getLanguage = function(){
        return CrimeMap.getLanguage();
    };
    $scope.language = $scope.getLanguage();

});
