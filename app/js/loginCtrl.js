crimeMapApp.controller('LoginCtrl', function ($scope, CrimeMap, $window) {
	
    $scope.signOut = function()  {
        CrimeMap.signOut();
        $window.location.href = "/#!home";  
    };
    $scope.signIn = function(email, password)  {
        console.log(CrimeMap.checkIfLoggedIn());
        CrimeMap.signIn(email, password);
        $window.location.href = "/#!home";  
    };

    $scope.checkIfLoggedIn = function(){
        console.log(CrimeMap.checkIfLoggedIn());
        return CrimeMap.checkIfLoggedIn();
    };

});
