//Controls the googlemap
crimeMapApp.controller('MapCtrl', function ($scope,CrimeMap, NgMap) {

    NgMap.getMap().then(function(map){
        $scope.map=map;
        CrimeMap.setMap($scope.map);
    });

    //Initialize parameters
    $scope.initCoordinates = CrimeMap.getInitCoordinates();
    $scope.currentEvent={"lat":0, "lng":0};

    //Retrieve events close to latitude longitude
    $scope.getCloseByEvents = function(lat,lng) {
        $scope.status = "Searching...";
        console.log($scope.status);
        CrimeMap.searchCloseTo.get({lat:lat, lng:lng},function(data){
            //Store events for this scope
            $scope.events=[];
            $scope.events=data.data;
            $scope.status = "Found! ";
            console.log($scope.status);
        },function(data){
            $scope.status = "There was an error";
            console.log(data);
        });
    };
    $scope.changeOpenedInfoWindow=function(e, id){
        if($scope.currentEvent!==0){
            $scope.map.hideInfoWindow($scope.currentEvent);
        }
        $scope.currentEvent=id;
    };
    $scope.showDetail = function(e, incident) {
        $scope.currentEvent=incident;
        $scope.currentEvent.pubdate_iso8601=$scope.currentEvent.pubdate_iso8601.replace("T", ", ").substring(0,17);
        $scope.currentEvent.content=  CrimeMap.removeTags($scope.currentEvent.content);
        $scope.map.showInfoWindow("eventinfo", incident.id);
    };

    $scope.getCloseByEvents($scope.initCoordinates[0], $scope.initCoordinates[1]);
});
