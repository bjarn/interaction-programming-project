Interaction Programing - Lab assignment - HTML
=================================================

Final project for KTH course Interaction programming.

To run a local version simply git clone this repository and run "npm start" in the directory.
The console will tell you at which ip-adress to access the webapp in a browser.
WARNING! There may be some problems running the npm startup script on Windows machines.
Simply starting the local CORS-proxy with "node node_modules/cors-proxy-server" and then the http-serv with "http-server app/ -a 0.0.0.0 -p 8000 --cors" usually is enough.

To test out the firebase storing of locations use the Username: llama@kth.se and Password: test1234.
There is currently no registration functionality.

Done with Lisa Lama
